﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UD2_Tarea3
{

    /**
    * Clase Rectangulo que hereda de Figura
    */
    internal class Rectangulo : Figura
    {
        private float baseRectangulo;
        private float alturaRectangulo;

        public Rectangulo(float baseRectangulo, float alturaRectangulo)
        {
            this.baseRectangulo = baseRectangulo;
            this.alturaRectangulo = alturaRectangulo;
        }

        /**
        * Calcula el perimetro de un rectangulo
        *
        * @return el resultado del calculo del perimetro
        */
        public override float Perimetro()
        {
            return 2 * (baseRectangulo + alturaRectangulo);
        }

        /**
        * Calcula el area de un rectangulo
        *
        * @return el resultado del calculo del area
        */
        public override float Area()
        {
            return baseRectangulo * alturaRectangulo;
        }


    }
}
