﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UD2_Tarea3
{
    /**
    * Clase que contiene los procesos del programa
    */
    internal class Aplicacion
    {
        private UtilES utilES;
        public Aplicacion()
        {
            utilES = new UtilES();
        }

        public void ejecutar()
        {
            mostrarMenu();
        }

        /**
        * Metodo que muestra el menú de opciones del programa
        */
        private void mostrarMenu()
        {
            int opcion;
            do
            {
                Console.WriteLine("0 - Salir");
                Console.WriteLine("1 - Rectangulo");
                Console.WriteLine("2 - Círculo");
                Console.WriteLine("3 – Triángulo");
                opcion = utilES.pideEntero("Introduza su opción: ");
                switch (opcion)
                {
                    case 0:
                        break;
                    case 1:
                        rectangulo();
                        esperar();
                        break;
                    case 2:
                        circulo();
                        esperar();
                        break;
                    case 3:
                        triangulo();
                        esperar();
                        break;
                }
            } while (opcion != 0);
        }

        /**
        * Metodo que pide por teclado la base y la altura de un rectangulo para mostrar
        * su perimetro y su area.
        */
        private void rectangulo()
        {
            float baseRectangulo = utilES.pideFloat("Introduce la base del Rectangulo");
            float alturaRectangulo = utilES.pideFloat("Introduce la altura del Rectangulo");
            Rectangulo rectangulo = new Rectangulo(baseRectangulo, alturaRectangulo);
            utilES.mensaje("El perimetro es: " + rectangulo.Perimetro());
            utilES.mensaje("El area es: " + rectangulo.Area());
        }

        /**
        * Metodo que pide por teclado el radio de un circulo para mostrar su perimetro
        * y su area.
        */
        private void circulo()
        {
            float radio = utilES.pideFloat("Introduce el radio del Círculo");
            Circulo circulo = new Circulo(radio);
            utilES.mensaje("El perimetro es: " +  circulo.Perimetro());
            utilES.mensaje("El area es: " + circulo.Area());
        }

        /**
        * Metodo que pide por teclado la base y la altura de un triangulo rectangulo para mostrar
        * su perimetro y su area.
        */
        private void triangulo()
        {
            float baseTriangulo = utilES.pideFloat("Introduce la base del Triángulo rectangulo");
            float alturaTriangulo = utilES.pideFloat("Introduce la altura del Triángulo rectangulo");
            Triangulo triangulo = new Triangulo(baseTriangulo, alturaTriangulo);
            utilES.mensaje("El perimetro es: " +  triangulo.Perimetro());
            utilES.mensaje("El area es: " + triangulo.Area());
        }

        /**
        * Metodo que pausa la ejecucion del programa a la espera de una accion por parte
        * del usuario, despues de la accion del usuario se limpia el contenido de la consola.
        */
        private void esperar()
        {
            Console.WriteLine("Pulsa INTRO para continuar...");
            Console.ReadLine();
            Console.Clear();
        }
    }
}
